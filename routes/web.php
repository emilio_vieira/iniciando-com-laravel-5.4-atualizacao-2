<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Meu primeiro Controller com Params
Route::get('route-controller/{param}','MyController@action');

// Meu primeiro Controller com Formul�rio e Valida��o CSRF (Cross-site request forgery)
Route::get('hello/{name?}','MyController@form');
Route::post('hello','MyController@render');
//Route::resource('pages','Admin\PagesController');


// GROUP AUTHENTICATION OLD - Simple Method
//Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
//    Route::resource('pages','Admin\PagesController');
//});


// GROUP AUTHENTICATION OPP - New Method
Route::prefix('admin')
    ->middleware('auth')
    ->group(function(){
        // Todos os recursos(index,create, edit e etc) ser�o permitidos ap�s autentica��o
        Route::resource('pages','Admin\PagesController');
    });


// AUTHENTION ROUTES
Auth::routes();

// HOME ROUTE NAMED
Route::get('/home', 'HomeController@index')->name('home');

/*
route::get('/', function () {
    return view('welcome');
});

// tipos de rotas
Route::get('route',function(){return '$_GET Route';});
Route::post('route',function(){
    // precisa do csrf_token() oriundo do formulario que gerou o action post
    $token = csrf_token();
    $htmt = <<<HTML
HTML;

    return '$_POST Route';
});

Route::delete('route',function(){return 'DELETE Route';});
Route::put('route',function(){return 'PUT Route';});
Route::patch('route',function(){return 'PATCH Route';});
Route::options('route',function(){return 'OPTIONS Route';});
Route::match(['get','post'],'match-route',function(){dump('Route $_GET OR $_POST');});

Route::get('params/{id?}/{name?}', function($id = "1",$name = "Emilio"){
   return "Route Params {$id} - {$name}";
});
*/



