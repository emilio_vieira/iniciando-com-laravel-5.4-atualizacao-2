<?php
/**
 * Created by PhpStorm.
 * User: emili
 * Date: 26-Jun-18
 * Time: 22:09
 */
?>

@if($name)
    <h1>Hello:{{ $name }}</h1>
@endif


<form action="/hello" method="post">
    {{ csrf_field() }}
    <label for="name">Nome</label>
    <input type="text" name="name" id="name">

    <button type="submit">Enviar</button>
</form>

