@component('admin.layouts.elements.body')
    @slot('title') Página @endslot
    @slot('description') Edição da Página @endslot

    <p><b>{{ $page->title }}</b></p>

    <form action="{{ route('pages.update',$page->id) }}" class="form-horizontal" method="POST">
        {{ method_field('PUT') }}
        @include('admin.pages.form')
    </form>

    <a href="{{ route('pages.show' , $page->id) }}" class="btn btn-default btn-xs ">Voltar</a>

@endcomponent