@component('admin.layouts.elements.body')
    @slot('title') Páginas @endslot
    @slot('description') Administração de Páginas @endslot

    <a href="{{ route('pages.create') }}" class="btn btn-default btn-xs ">Novo</a>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>title</th>
            <th class="text-right">ação</th>
        </tr>
        </thead>
        <tbody>
            @foreach($pages as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->title }}</td>
                    <td class="text-right">

                        <a href="{{ route('pages.show' , $p->id) }}" class="btn btn-default btn-xs">
                            <span class="glyphicon glyphicon-plus"></span>
                        </a>

                        {{--<form action="{{ route('pages.destroy',$p->id  )}}" id="form-delete-pages-{{ $p->id }}">--}}
                            {{--{{ method_field('DELETE') }}--}}
                            {{--<a href="#" data-title="{{ $p->title }}" data-form="pages-{{ $p->id }}" class="data-delete">--}}
                                {{--<i class="glyphicon glyphicon-remove icon-spacer"></i>--}}
                            {{--</a>--}}
                        {{--</form>--}}

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $pages->links() }}
@endcomponent




<script>
    jQuery(document).ready(function(){
        jQuery('.data-delete').click( function (e) {

            if(!confirm('Deseja realmente excluir '+ jQuery(this).data('title')+' ?'))
                return;

            e.preventDefault();
            jQuery('#form-delete-'+jQuery(this).data('form')).submit();

        })
    })
</script>