@component('admin.layouts.elements.body')
    @slot('title') Página @endslot
    @slot('description') Exibição da Página @endslot

    <h4>{{ $page->title }}</h4>
    <p><smal>Criando em {{ $page->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $page->updated_at->format('d/m/Y H:i:s') }}</smal></p>

    {{--{{ $page->body }} --}}{{-- htmlentities / escapa caracteres e exibe as tags / perigoso, passivel a SQLInject --}}
    {!! $page->body !!} {{--html_entity_decode / converte entidades html para caracteres   --}}
    <a href="{{ route('pages.index') }}" class="btn btn-default btn-xs ">Voltar</a>
    <a href="{{ route('pages.edit' , $page->id) }}" class="btn btn-default btn-xs">Editar</a>

    <form action="{{ route('pages.destroy',$page->id  )}}" class="form-horizontal" method="post" style="display:inline-block">
        {!! csrf_field() !!}
        {!! method_field('DELETE') !!}
        <input type="submit" value="remover" class="btn btn-xs btn-default">
    </form>

@endcomponent

