@component('admin.layouts.elements.body')
    @slot('title') Páginas @endslot
    @slot('description') Criação de Páginas @endslot

    <form action="{{ route('pages.store') }}" class="form-horizontal" method="POST">
        @include('admin.pages.form')
    </form>

    <a href="{{ route('pages.index') }}" class="btn btn-default btn-xs ">Voltar</a>

@endcomponent


