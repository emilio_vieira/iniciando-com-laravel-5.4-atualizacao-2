<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    //
    public function action(Request $request,$param){
//        dump($request->all());
        return view('myController/action',compact('param'));
    }


    public function form($name = 'Emilio'){
        return view('myController/form',compact('name'));
    }

    public function render(Request $request){
        return $request->input('name');
    }
}
